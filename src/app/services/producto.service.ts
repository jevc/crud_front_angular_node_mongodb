import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Producto } from '../models/producto';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ProductoService {

  url = 'http://localhost:5000/api/productos/';

  constructor( protected clienteHTTP: HttpClient ) { }  

  getProductos() : Observable<any> {
    return this.clienteHTTP.get(this.url);
  }

  eliminarProducto(id: string): Observable<any> {
    return this.clienteHTTP.delete(this.url + id);
  }

  guardarProducto(producto: Producto): Observable<any> {
    return this.clienteHTTP.post(this.url, producto);
  }

  obtenerProducto(id: string): Observable<any> {
    return this.clienteHTTP.get(this.url + id);
  }
  
  editarProducto(id:string, producto: Producto): Observable<any> {
    return this.clienteHTTP.put(this.url + id, producto);
  }
}
