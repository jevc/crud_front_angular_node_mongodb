import { Component, OnInit } from '@angular/core';
import { ProductoService } from '../../services/producto.service';
import { Producto } from '../../models/producto';
import { log } from 'util';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-listar-productos',
  templateUrl: './listar-productos.component.html',
  styleUrls: ['./listar-productos.component.css']
})
export class ListarProductosComponent implements OnInit {

  listProductos: Producto[] = [];

  constructor(
    private _productoService: ProductoService,
    private toastr: ToastrService,
    private spinnerService: NgxSpinnerService
   )
   { 
    
   }

  ngOnInit() {

    this.obtenerProductos();

  }

  obtenerProductos() {

    this.spinnerService.show();    

    this._productoService.getProductos().subscribe( data => {
      
      this.listProductos = data;      
      this.spinnerService.hide();

    }, error => {
      this.spinnerService.hide();
      console.log(error);      
    });

  }

  eliminarProducto(id: any) {

    this._productoService.eliminarProducto(id).subscribe( data => {
      
      this.toastr.error('El producto fue eliminado con exito', 'Producto eliminado');
      this.obtenerProductos();

    }, error => {
      console.log(error);      
    });
  }

}
