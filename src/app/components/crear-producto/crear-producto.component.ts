import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Producto } from '../../models/producto';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ProductoService } from '../../services/producto.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-crear-producto',
  templateUrl: './crear-producto.component.html',
  styleUrls: ['./crear-producto.component.css']
})
export class CrearProductoComponent implements OnInit {

  productoForm : FormGroup;
  titulo= 'Crear producto';
  id: string | null;

  constructor( 
      private fb: FormBuilder, 
      private router: Router,
      private toastr: ToastrService,
      private _productoService: ProductoService,
      private aRouter: ActivatedRoute,
      private spinnerService: NgxSpinnerService
     ) { 

    this.productoForm = this.fb.group( {
      nombre:['', Validators.required],
      categoria:['', Validators.required],
      ubicacion:['', Validators.required],
      precio:['', Validators.required]
    });

    this.id = this.aRouter.snapshot.paramMap.get('id');

  }

  ngOnInit() {
    this.esEditar();
  }

  agregarProducto() {
    
    const producto: Producto = {
      nombre:this.productoForm.get('nombre').value,
      categoria:this.productoForm.get('categoria').value,
      ubicacion:this.productoForm.get('ubicacion').value,
      precio:this.productoForm.get('precio').value,      
    }

    this.spinnerService.show();
    
    if (this.id != null) {  // editamos producto
      
      this._productoService.editarProducto(this.id, producto).subscribe(data => {
        this.spinnerService.hide();
        this.toastr.success("El producto fue actualizado con éxito", "Producto actualizado");
        this.router.navigate(['/']);  
      }, error => {
        this.spinnerService.hide();
        console.log(error);      
        this.productoForm.reset();
      });

    } else {  // agregamos producto
      
      this._productoService.guardarProducto(producto).subscribe(data => {

        this.spinnerService.hide();
        this.toastr.success('El producto fue registado con exito!', 'Producto registrado');
        this.router.navigate(['/']);
  
      }, error => {
        this.spinnerService.hide();
        console.log(error);      
        this.productoForm.reset();
      });

    }          
          
  }

  esEditar() {

    if (this.id != null) {

      this.spinnerService.show();
      this.titulo = "Editar producto";
      
      this._productoService.obtenerProducto(this.id).subscribe( data => {

        this.productoForm.setValue({
          nombre:data.nombre,
          categoria:data.categoria,
          ubicacion: data.ubicacion,
          precio:data.precio,  
        });

        this.spinnerService.hide();

      })

    }
  }

}
